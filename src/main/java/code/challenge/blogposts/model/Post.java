package code.challenge.blogposts.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Post {
    private int id, userId;
    private String title, body;
    public String getTitle() {
        return title;
    }
}


