# blogposts-challenge

## Setup

### 1- Install the server dependencies:

`mvn install`


### 2- Copy the static client to the static directory :

`src\main\resources\static`

### 3- Run the project:

`mvn spring-boot:run`

### 4- Build the project :
`mvn package`


### 4- Run a standalone jar :
`java -jar target/blogposts-0.0.1-SNAPSHOT.jar`

### Author
[Saad Regal](https://github.com/SaadRegal/)

### Version

1.0.0
