package code.challenge.blogposts.controller;

import code.challenge.blogposts.model.Post;
import com.google.gson.Gson;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
public class PostController {
    private Post[] posts;
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/api", method = RequestMethod.GET)
    public String getPosts() {
        this.retrievePost();
        this.limitPosts();
        this.sortPosts();
        Gson gson = new Gson();
        return gson.toJson(posts);
    }

    public void retrievePost() {
        final String uri = "https://jsonplaceholder.typicode.com/posts/";
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(uri, String.class);

        Gson gson = new Gson();
        posts = gson.fromJson(response, Post[].class);
    }

    private void limitPosts() {
        int limit = 50;
        int i = 0;
        Post[] limitedPosts = new Post[limit];
        while (i < limit) {
            limitedPosts[i] = posts[i];
            i++;
        }
        posts = limitedPosts;
    }


    private void sortPosts() {
        int length = posts.length;
        Post tmp;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (posts[i].getTitle().compareTo(posts[j].getTitle()) > 0) {
                    tmp = posts[i];
                    posts[i] = posts[j];
                    posts[j] = tmp;
                }
            }
        }
    }

}
